package main

import (
	"context"
	"fmt"

	"github.com/fanchann/test-kazoku/internals/models"
	"github.com/fanchann/test-kazoku/internals/usecases"
)

func main() {
	// cfg := config.NewViper()
	// logger := config.NewLogger()
	// db := config.NewGorm(cfg, logger)
	// validate := validator.New()

	// userRepo := repositories.NewUsersRepository(logger)

	// userUsecase := usecases.NewUsersUsecase(db, userRepo, validate, logger)
	userFileUsecase := usecases.NewUserFilesUsecase(nil, nil, nil, nil)
	modelUserfile := models.CreateUserFilesUsecaseRequest{FileName: "q.png"}
	ufur, err := userFileUsecase.AddNewFile(context.Background(), &modelUserfile)
	if err != nil {
		panic(err)
	}
	fmt.Printf("ufur: %v\n", ufur)

	// userModel := models.CreateUserUsecaseRequest{Name: "P", Password: "anjing", Email: "emoefienfirv@gmail.com", Address: "ofjeojfoe"}

	// uur, err := userUsecase.CreateUser(context.Background(), &userModel)
	// if err != nil {
	// 	panic(err)
	// }

	// fmt.Printf("uur: %v\n", uur)

	// uur, err := userUsecase.FindUserById(context.Background(), 2)
	// if err != nil {
	// 	panic(err)
	// }

	// fmt.Printf("uur: %v\n", uur)
	// fmt.Printf("converter.UserCreateModelToEntity(userModel): %v\n", converter.UserCreateModelToEntity(&userModel))

	// readFile()
}
