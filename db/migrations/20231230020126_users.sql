-- migrate:up
create table users(
    user_id INTEGER NOT NULL auto_increment PRIMARY KEY,
    user_name VARCHAR(100) NOT NULL,
    user_email VARCHAR(255) NOT NULL UNIQUE,
    user_password VARCHAR(255) NOT NULL,
    user_address text
);

-- migrate:down
DROP TABLE if EXISTS users;
