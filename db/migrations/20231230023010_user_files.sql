-- migrate:up
-- migrate:up
create table user_files(
    file_id INTEGER NOT NULL PRIMARY KEY auto_increment,
    user_id INTEGER NOT NULL,
    file_name VARCHAR(25) NOT NULL,
    Foreign Key (user_id) REFERENCES users(user_id)
);

-- migrate:down
DROP TABLE if EXISTS user_files;


