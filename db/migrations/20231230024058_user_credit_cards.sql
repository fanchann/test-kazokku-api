-- migrate:up
CREATE TABLE user_credit_card (
    card_id INTEGER PRIMARY KEY NOT NULL auto_increment,
    user_id INTEGER NOT NULL,
    creditcard_type VARCHAR(50) NOT NULL,
    creditcard_number VARCHAR(16) NOT NULL,
    creditcard_name VARCHAR(100) NOT NULL,
    creditcard_expired DATE NOT NULL,
    creditcard_cvv VARCHAR(4) NOT NULL,
    Foreign Key (user_id) REFERENCES users(user_id)
);


-- migrate:down
DROP TABLE if EXISTS user_credit_card;

