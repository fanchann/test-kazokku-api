package entities

import "time"

type UserCreditCard struct {
	CardID            int       `gorm:"column:card_id"`
	UserID            int       `gorm:"column:user_id"`
	CreditCardType    string    `gorm:"column:creditcard_type"`
	CreditCardNumber  string    `gorm:"column:creditcard_number"`
	CreaditCardName   string    `gorm:"column:creditcard_name"`
	CreditCardExpired time.Time `gorm:"column:creditcard_expired"`
	CreditCardCvv     uint16    `gorm:"column:creditcard_cvv"`
	Users             Users     `gorm:"foreignKey:user_id;references:user_id"`
}

func (ucr *UserCreditCard) TableName() string {
	return "user_credit_card"
}
