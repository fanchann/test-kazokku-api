package entities

type UserFiles struct {
	FileID   int    `gorm:"column:file_id;autoIncrement;primaryKey"`
	UserID   int    `gorm:"column:user_id"`
	FileName string `gorm:"file_name"`
	Users    Users  `gorm:"foreignKey:user_id;references:user_id"`
}

func (uf *UserFiles) TableName() string {
	return "user_files"
}
