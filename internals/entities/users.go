package entities

import (
	"log"

	"golang.org/x/crypto/bcrypt"
)

var (
	costIntPassword = 16
)

type Users struct {
	UserID         int             `gorm:"column:user_id;primaryKey;autoIncrement"`
	UserName       string          `gorm:"column:user_name"`
	UserEmail      string          `gorm:"column:user_email;unique"`
	UserPassword   string          `gorm:"column:user_password"`
	UserAddress    string          `gorm:"column:user_address"`
	UserFiles      *[]UserFiles    `gorm:"foreignKey:user_id;references:user_id"`
	UserCreditCard *UserCreditCard `gorm:"foreignKey:user_id;references:user_id"`
}

func (u *Users) TableName() string {
	return "users"
}

func (u *Users) HashPassword(password string) error {
	bytePass, err := bcrypt.GenerateFromPassword([]byte(password), costIntPassword)
	if err != nil {
		log.Printf("error :", err)
		return err
	}
	u.UserPassword = string(bytePass)
	return nil
}

func (u *Users) CompareAndHashPassword(password string) error {
	if err := bcrypt.CompareHashAndPassword([]byte(u.UserPassword), []byte(password)); err != nil {
		log.Printf("error :", err)
		return err
	}

	return nil
}
