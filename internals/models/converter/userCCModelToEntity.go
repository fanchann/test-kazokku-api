package converter

import (
	"github.com/fanchann/test-kazoku/internals/entities"
	"github.com/fanchann/test-kazoku/internals/models"
)

func UserCCModelToEntityCreate(req *models.CreateUserCreditCardRequest) *entities.UserCreditCard {
	return &entities.UserCreditCard{
		UserID:            req.UserID,
		CreditCardType:    req.CreditCardType,
		CreditCardNumber:  req.CreditCardNumber,
		CreaditCardName:   req.CreditCardName,
		CreditCardExpired: req.CreditCardExpired,
		CreditCardCvv:     req.CreditCardCvv,
	}
}

func UserCCModelToEntityUpdate(req *models.UpdateUserCreditCardRequest) *entities.UserCreditCard {
	return &entities.UserCreditCard{
		CreditCardType:    req.CreditCardType,
		CreditCardNumber:  req.CreditCardNumber,
		CreaditCardName:   req.CreditCardName,
		CreditCardExpired: req.CreditCardExpired,
		CreditCardCvv:     req.CreditCardCvv,
	}
}
