package converter

import (
	"log"

	"github.com/fanchann/test-kazoku/internals/entities"
	"github.com/fanchann/test-kazoku/internals/models"
)

func UserCreateModelToEntity(req *models.CreateUserUsecaseRequest) *entities.Users {
	userEntities := new(entities.Users)
	userEntities.UserName = req.Name
	userEntities.UserEmail = req.Email
	userEntities.UserAddress = req.Address
	if err := userEntities.HashPassword(req.Password); err != nil {
		log.Printf("error : %%v", err)
	}

	return userEntities
}
