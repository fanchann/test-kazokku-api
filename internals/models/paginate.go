package models

type Pagination struct {
	Limit      int
	Page       int
	Sort       string
	OrderBy    string
	TotalRows  int64
	TotalPages int
	Rows       interface{}
}

func (p *Pagination) GetOffset() int {
	return (p.GetPage() - 1) * p.GetLimit()
}

func (p *Pagination) GetLimit() int {
	if p.Limit == 0 {
		p.Limit = 30
	}
	return p.Limit
}

func (p *Pagination) GetPage() int {
	if p.Page == 0 {
		p.Page = 1
	}
	return p.Page
}

func (p *Pagination) GetOrderBy() string {
	if p.OrderBy == "name" || p.OrderBy == "" {
		p.OrderBy = "name"
	}

	if p.OrderBy == "email" {
		p.OrderBy = "email"
	}

	return p.OrderBy
}

func (p *Pagination) GetSort() string {
	if p.Sort == "desc" {
		p.Sort = "user_id desc"
	}

	if p.Sort == "asc" || p.Sort == "" {
		p.Sort = "user_id asc"
	}
	return p.Sort
}
