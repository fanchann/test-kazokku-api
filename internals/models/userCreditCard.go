package models

import "time"

type UserCreditCardResponse struct {
	CreditCardType    string `json:"creditcard_type"`
	CreditCardNumber  string `json:"creditcard_number"`
	CreditCardName    string `json:"creditcard_name"`
	CreditCardExpired string `json:"creditcard_exp"`
	CreditCardCvv     string `json:"creditcard_cvv"`
}

type CreateUserCreditCardRequest struct {
	UserID            int
	CreditCardType    string    `validate:"gte=4"`
	CreditCardNumber  string    `validate:"creditcard_number"`
	CreditCardName    string    `validate:"gte=4"`
	CreditCardExpired time.Time `validate:"required"`
	CreditCardCvv     uint16    `validate:"len=3"`
}

type UpdateUserCreditCardRequest struct {
	UserID            int
	CreditCardType    string    `validate:"required;gte=4"`
	CreditCardNumber  string    `validate:"creditcard_number"`
	CreditCardName    string    `validate:"required;gte=4"`
	CreditCardExpired time.Time `validate:"required"`
	CreditCardCvv     uint16    `validate:"required;len=3"`
}
