package models

type UserFilesUsecaseResponse struct {
	Files string
}

type CreateUserFilesUsecaseRequest struct {
	FileName string
	UserID   int
}

type UpdateUserFilesUsecaseRequest struct {
	UserID    int
	FileLists []string
}
