package models

type UserUsecaseResponse struct {
	UserID  int    `json:"user_id"`
	Name    string `json:"name"`
	Email   string `json:"email"`
	Address string `json:"address"`
}

type UpdateUserUsecaseRequest struct {
	UserID   int
	Name     string
	Email    string `validate:"email"`
	Password string
	Address  string
}

type CreateUserUsecaseRequest struct {
	Name     string `validate:"required"`
	Address  string `validate:"required"`
	Email    string `validate:"required,email"`
	Password string `validate:"required"`
}
