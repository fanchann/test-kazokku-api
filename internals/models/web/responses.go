package web

import "github.com/fanchann/test-kazoku/internals/models"

type WebUserCCAndFiles struct {
	UserID     int
	Name       string
	Email      string
	Address    string
	Photos     []string
	CreditCard models.UserCreditCardResponse
}
