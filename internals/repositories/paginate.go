package repositories

import (
	"math"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"

	"github.com/fanchann/test-kazoku/internals/entities"
	"github.com/fanchann/test-kazoku/internals/models"
)

type PaginationLists struct {
	Log *logrus.Logger
}

func (p *PaginationLists) Lists(db *gorm.DB, pagination models.Pagination) (*models.Pagination, error) {
	var data *[]entities.Users
	db.Scopes(paginate(data, &pagination, db)).Find(&data)
	pagination.Rows = data

	return &pagination, nil
}

func paginate(value interface{}, pagination *models.Pagination, db *gorm.DB) func(db *gorm.DB) *gorm.DB {
	var totalRows int64
	db.Model(value).Count(&totalRows)

	pagination.TotalRows = totalRows
	totalPages := int(math.Ceil(float64(totalRows) / float64(pagination.Limit)))
	pagination.TotalPages = totalPages

	return func(db *gorm.DB) *gorm.DB {
		return db.Offset(pagination.GetOffset()).Limit(pagination.GetLimit()).Order(pagination.GetSort()).Order(pagination.GetOrderBy())
	}
}
