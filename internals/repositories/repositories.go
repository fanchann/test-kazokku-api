package repositories

import (
	"fmt"

	"gorm.io/gorm"
)

type Repositories[T any] struct {
	DB *gorm.DB
}

func (r *Repositories[T]) Create(db *gorm.DB, entity *T) error {
	return db.Create(entity).Error
}

func (r *Repositories[T]) Update(db *gorm.DB, entity *T) error {
	return db.Save(entity).Error
}

// func (r *Repositories[T]) Delete(db *gorm.DB, entity *T) error {
// 	return db.Delete(entity).Error
// }

func (r *Repositories[T]) CountById(db *gorm.DB, id any, columnName string) (int64, error) {
	var total int64
	err := db.Model(new(T)).Where(fmt.Sprintf("%s = ?", columnName), id).Count(&total).Error
	return total, err
}

func (r *Repositories[T]) FindById(db *gorm.DB, entity *T, id any, columnName string) error {
	return db.Where(fmt.Sprintf("%s = ?", columnName), id).Take(entity).Error
}
