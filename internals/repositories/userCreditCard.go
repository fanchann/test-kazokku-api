package repositories

import (
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"

	"github.com/fanchann/test-kazoku/internals/entities"
)

type UserCreditCardRepositories struct {
	Repositories[entities.UserCreditCard]
	Log *logrus.Logger
}

func (u *UserCreditCardRepositories) FindCreditCardByNumber(db *gorm.DB, creditCard *entities.UserCreditCard, creditCardNum string) error {
	return db.Where("creditcard_number = ?", creditCardNum).Take(creditCard).Error
}
