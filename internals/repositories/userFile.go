package repositories

import (
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"

	"github.com/fanchann/test-kazoku/internals/entities"
)

type UserFilesRepository struct {
	Repositories[entities.UserFiles]
	Log *logrus.Logger
}

func NewUserFilesRepository(log *logrus.Logger) *UserFilesRepository {
	return &UserFilesRepository{Log: log}
}

func (uf *UserFilesRepository) FindFilesUserByUserID(db *gorm.DB, userFiles *[]entities.UserFiles, userId string) error {
	return db.Where("user_id = ?", userId).Find(userFiles).Error
}
