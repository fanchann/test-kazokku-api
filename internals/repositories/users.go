package repositories

import (
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"

	"github.com/fanchann/test-kazoku/internals/entities"
)

type Users struct {
	Repositories[entities.Users]
	Log *logrus.Logger
}

func NewUsersRepository(log *logrus.Logger) *Users {
	return &Users{Log: log}
}

func (u *Users) FindByEmail(db *gorm.DB, user *entities.Users, email string) error {
	return db.Where("user_email = ?", email).First(user).Error
}
