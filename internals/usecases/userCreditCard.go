package usecases

import (
	"context"
	"strconv"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"

	"github.com/fanchann/test-kazoku/internals/entities"
	"github.com/fanchann/test-kazoku/internals/models"
	"github.com/fanchann/test-kazoku/internals/models/converter"
	"github.com/fanchann/test-kazoku/internals/repositories"
)

type IUserCreditCardUsecases interface {
	CreateNewCreditCard(ctx context.Context, req *models.CreateUserCreditCardRequest) (*models.UserCreditCardResponse, error)
	UpdateCreditCard(ctx context.Context, req *models.UpdateUserCreditCardRequest) (*models.UserCreditCardResponse, error)
}

type UserCreditCardUsecases struct {
	DB                 *gorm.DB
	Log                *logrus.Logger
	UserCreditCardRepo *repositories.UserCreditCardRepositories
	Validate           *validator.Validate
}

func (u *UserCreditCardUsecases) CreateNewCreditCard(ctx context.Context, req *models.CreateUserCreditCardRequest) (*models.UserCreditCardResponse, error) {
	tx := u.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := u.Validate.Struct(req); err != nil {
		return nil, fiber.ErrBadRequest
	}

	ccReq := converter.UserCCModelToEntityCreate(req)
	if err := u.UserCreditCardRepo.Create(tx, ccReq); err != nil {
		u.Log.Warnf("error :%v", err)
		return nil, fiber.ErrInternalServerError
	}
	if err := tx.Commit().Error; err != nil {
		u.Log.Warnf("error :%v", err)
		return nil, fiber.ErrInternalServerError
	}

	ccCvv := strconv.Itoa(int(ccReq.CreditCardCvv))
	return &models.UserCreditCardResponse{
		CreditCardType:    ccReq.CreditCardType,
		CreditCardNumber:  ccReq.CreditCardNumber,
		CreditCardName:    ccReq.CreaditCardName,
		CreditCardExpired: ccReq.CreditCardExpired.String(),
		CreditCardCvv:     ccCvv,
	}, nil

}

func (u *UserCreditCardUsecases) UpdateCreditCard(ctx context.Context, req *models.UpdateUserCreditCardRequest) (*models.UserCreditCardResponse, error) {
	tx := u.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	userCCData := new(entities.UserCreditCard)
	if err := u.UserCreditCardRepo.FindById(tx, userCCData, req.UserID, "user_id"); err != nil {
		u.Log.Warnf("data not found : %v", err)
		return nil, fiber.ErrNotFound
	}

	ccReq := converter.UserCCModelToEntityUpdate(req)
	if err := u.UserCreditCardRepo.Create(tx, ccReq); err != nil {
		u.Log.Warnf("error :%v", err)
		return nil, err
	}

	if err := tx.Commit().Error; err != nil {
		u.Log.Warnf("error :%v", err)
	}

	ccCvv := strconv.Itoa(int(ccReq.CreditCardCvv))
	return &models.UserCreditCardResponse{
		CreditCardType:    ccReq.CreditCardType,
		CreditCardNumber:  ccReq.CreditCardNumber,
		CreditCardName:    ccReq.CreaditCardName,
		CreditCardExpired: ccReq.CreditCardExpired.String(),
		CreditCardCvv:     ccCvv,
	}, nil
}
