package usecases

import (
	"context"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"

	"github.com/fanchann/test-kazoku/internals/entities"
	"github.com/fanchann/test-kazoku/internals/models"
	"github.com/fanchann/test-kazoku/internals/repositories"
	"github.com/fanchann/test-kazoku/utils"
)

type UserFilesUsecase struct {
	DB        *gorm.DB
	Log       *logrus.Logger
	UserFile  *repositories.UserFilesRepository
	Validator *validator.Validate
}

func NewUserFilesUsecase(db *gorm.DB, log *logrus.Logger, userFile *repositories.UserFilesRepository, validator *validator.Validate) *UserFilesUsecase {
	return &UserFilesUsecase{DB: db, Log: log, UserFile: userFile, Validator: validator}
}

func (u *UserFilesUsecase) AddNewFile(ctx context.Context, req *models.CreateUserFilesUsecaseRequest) (*models.UserFilesUsecaseResponse, error) {
	tx := u.DB.Begin()
	defer tx.Rollback()

	userFileReq := new(entities.UserFiles)
	userFileReq.UserID = req.UserID
	userFileReq.FileName = utils.IdGenerator()

	if err := u.UserFile.Repositories.Create(tx, userFileReq); err != nil {
		u.Log.Warnf("error while insert : %v", err)
		return nil, fiber.ErrInternalServerError
	}

	if err := tx.Commit().Error; err != nil {
		return nil, fiber.ErrInternalServerError
	}
	return &models.UserFilesUsecaseResponse{
		Files: userFileReq.FileName,
	}, nil
}
