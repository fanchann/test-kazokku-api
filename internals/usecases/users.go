package usecases

import (
	"context"
	"errors"
	"strconv"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"

	"github.com/fanchann/test-kazoku/internals/entities"
	"github.com/fanchann/test-kazoku/internals/models"
	"github.com/fanchann/test-kazoku/internals/models/converter"
	"github.com/fanchann/test-kazoku/internals/models/web"
	"github.com/fanchann/test-kazoku/internals/repositories"
)

type IUsersUsecase interface {
	CreateUser(ctx context.Context, req *models.CreateUserUsecaseRequest) (*models.UserUsecaseResponse, error)
	FindUserById(ctx context.Context, id int) (*models.UserUsecaseResponse, error)
	UpdateUser(ctx context.Context, req *models.UpdateUserUsecaseRequest) (*models.UserUsecaseResponse, error)
	FindUserWithCCAndFiles(ctx context.Context, idUser int) (*web.WebUserCCAndFiles, error)
}

type UsersUsecase struct {
	DB       *gorm.DB
	UserRepo *repositories.Users
	Validate *validator.Validate
	Log      *logrus.Logger
}

func NewUsersUsecase(db *gorm.DB, userRepo *repositories.Users, validate *validator.Validate, log *logrus.Logger) *UsersUsecase {
	return &UsersUsecase{
		DB:       db,
		UserRepo: userRepo,
		Validate: validate,
		Log:      log,
	}
}

func (u *UsersUsecase) CreateUser(ctx context.Context, req *models.CreateUserUsecaseRequest) (*models.UserUsecaseResponse, error) {
	tx := u.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := u.Validate.Struct(req); err != nil {
		u.Log.Warnf("Failed register %v", err)
		return nil, fiber.ErrBadRequest
	}

	// find email
	userData := new(entities.Users)
	if err := u.UserRepo.FindByEmail(tx, userData, req.Email); err != nil {
		u.Log.Warnf("Duplite email %v", err)
		return nil, errors.New("duplicate email")
	}

	request := converter.UserCreateModelToEntity(req)
	if err := u.UserRepo.Create(tx, request); err != nil {
		u.Log.Warnf("Error while insert to db : %v", err)
		return nil, fiber.ErrInternalServerError
	}

	if err := tx.Commit().Error; err != nil {
		u.Log.Warnf("Failed commit transaction : %v", err)
		return nil, fiber.ErrInternalServerError
	}

	return &models.UserUsecaseResponse{
		UserID:  request.UserID,
		Name:    request.UserName,
		Email:   request.UserEmail,
		Address: request.UserAddress,
	}, nil

}

func (u *UsersUsecase) FindUserById(ctx context.Context, id int) (*models.UserUsecaseResponse, error) {
	tx := u.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	userData := new(entities.Users)
	err := u.UserRepo.FindById(tx, userData, id, "user_id")
	if err != nil || userData == nil {
		return nil, fiber.ErrNotFound
	}
	if err := tx.Commit().Error; err != nil {
		u.Log.Warnf("Failed commit transaction : %v", err)
		return nil, fiber.ErrInternalServerError
	}
	return &models.UserUsecaseResponse{
		UserID:  userData.UserID,
		Name:    userData.UserName,
		Email:   userData.UserEmail,
		Address: userData.UserAddress,
	}, nil
}

func (u *UsersUsecase) UpdateUser(ctx context.Context, req *models.UpdateUserUsecaseRequest) (*models.UserUsecaseResponse, error) {
	tx := u.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := u.Validate.Struct(req); err != nil {
		u.Log.Warnf("%v", err)
		return nil, fiber.ErrBadRequest
	}

	userData := new(entities.Users)
	if err := u.UserRepo.FindById(tx, userData, req.UserID, "user_id"); err != nil {
		u.Log.Warnf("Data not found : %v", err)
		return nil, fiber.ErrNotFound
	}

	userData.UserID = req.UserID
	userData.UserName = req.Name
	userData.UserAddress = req.Address
	userData.UserEmail = req.Email
	userData.UserPassword = req.Password

	if err := u.UserRepo.Update(tx, userData); err != nil {
		u.Log.Warnf("error while update data : %v", err)
		return nil, err
	}

	if err := tx.Commit().Error; err != nil {
		u.Log.Warnf("Failed commit transaction : %v", err)
		return nil, fiber.ErrInternalServerError
	}
	return &models.UserUsecaseResponse{
		UserID:  userData.UserID,
		Name:    userData.UserName,
		Email:   userData.UserEmail,
		Address: userData.UserAddress,
	}, nil
}

func (u *UsersUsecase) FindUserWithCCAndFiles(ctx context.Context, idUser int) (*web.WebUserCCAndFiles, error) {
	tx := u.DB.Begin()
	defer tx.Rollback()

	var userWithCCAndFile entities.Users

	if err := u.DB.Model(&entities.Users{}).Where("user_id = ?", idUser).Preload("UserFiles").Preload("UserCreditCard").Find(&userWithCCAndFile).Error; err != nil {
		return nil, fiber.ErrNotFound
	}

	if err := tx.Commit().Error; err != nil {
		return nil, fiber.ErrInternalServerError
	}

	cvvStr := strconv.Itoa(int(userWithCCAndFile.UserCreditCard.CreditCardCvv))

	return &web.WebUserCCAndFiles{
		UserID: userWithCCAndFile.UserID,
		Name:   userWithCCAndFile.UserName,
		Email:  userWithCCAndFile.UserEmail,
		Photos: nil,
		CreditCard: models.UserCreditCardResponse{
			CreditCardType:    userWithCCAndFile.UserCreditCard.CreditCardType,
			CreditCardNumber:  userWithCCAndFile.UserCreditCard.CreditCardNumber,
			CreditCardName:    userWithCCAndFile.UserCreditCard.CreaditCardName,
			CreditCardExpired: userWithCCAndFile.UserCreditCard.CreditCardExpired.String(),
			CreditCardCvv:     cvvStr,
		},
	}, nil
}



