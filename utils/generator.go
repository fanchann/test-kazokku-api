package utils

import (
	"fmt"
	"strings"

	"github.com/google/uuid"
)

func IdGenerator() string {
	id := uuid.NewString()
	idWithoutHyphens := strings.ReplaceAll(id, "-", "")
	return idWithoutHyphens
}

func main() {
	idUser := IdGenerator()
	idFile := IdGenerator()

	fmt.Printf("idFile: %v\n", idFile)
	fmt.Printf("idUser: %v\n", idUser)
}
